
**just a demonstration only on nginx image and 1 html file**

---
## Step for deploying on CCE for testing (Bitbucket Pipelines)

  
after clone

1. add your id,password for loging in to your registry in your cluster ([ref](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/))
```shell
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>

```

 2. add all environment variables on bitbucket repository (I grab user and password from [this](https://support.huaweicloud.com/intl/en-us/usermanual-swr/swr_01_1000.html))
	- $SWR_USER
	- $SWR_PSSWRD
	-  $KUBECONFIG
		- download the kubeconfig.json from your cluster, then encrypt it with base64 to store it as a value
 3. work now?

  

---

  

## Pipline

1. Build app

2. Build image

3. Deploy on CCE



---
ping me if there is any problem, this doc is written after I completed it.
there might be some steps I missed to write here.